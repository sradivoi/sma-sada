package radivoi.sasa.prezenta;

import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import android.util.Log;

import com.raizlabs.android.dbflow.sql.language.SQLite;

import org.junit.Test;
import org.junit.runner.RunWith;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import radivoi.sasa.prezenta.domain.entity.Course;
import radivoi.sasa.prezenta.domain.entity.Student;
import radivoi.sasa.prezenta.domain.entity.Student_Table;
import radivoi.sasa.prezenta.domain.repository.CourseRepository;

import static org.junit.Assert.assertEquals;

/**
 * Instrumentation test, which will execute on an Android device.
 *
 * @see <a href="http://d.android.com/tools/testing">Testing documentation</a>
 */
@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTest {
    private static String DATE_FORMAT = "dd-MM-yyyy";

    @Test
    public void useAppContext() throws Exception {
        // Context of the app under test.
        Context appContext = InstrumentationRegistry.getTargetContext();

        assertEquals("radivoi.sasa.prezenta", appContext.getPackageName());
    }

    @Test
    public void testStatisticsQuery() throws Exception {

        Date fromDate = transformToDate("06-01-2017",DATE_FORMAT);
        Date toDate = transformToDate("09-01-2017",DATE_FORMAT);
        List<Course> list= new CourseRepository().getCourseBetween(fromDate, toDate, "Sasa", "Radivoi");
        System.out.println("Number of attendance: " + list.size());
        Log.d("TEST: ", "Number of attendance: " + list.size());
    }

    @Test
    public void testSelectAllQuery() throws Exception {

        List<Student> list= SQLite.select()
                .from(Student.class)
                .queryList();
        List<Course> list2= SQLite.select()
                .from(Course.class)
                .queryList();
        System.out.println("Number of attendance: " + list.size());
        Log.d("TEST: ", "Number of students: " + list.size());
    }

    private Date transformToDate(String date, String formatPattern) {
        SimpleDateFormat format = new SimpleDateFormat(formatPattern, Locale.ENGLISH);
        java.util.Date parsed = null;
        try {
            parsed = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert parsed != null;
        return new java.sql.Date(parsed.getTime());
    }
}
