package radivoi.sasa.prezenta.service.presentation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import radivoi.sasa.prezenta.R;
import radivoi.sasa.prezenta.domain.entity.Student;

/**
 * Created by I335330 on 08/01/2017.
 */

public class StudentAdaptor extends ArrayAdapter<Student> {
    public StudentAdaptor(Context context, ArrayList<Student> courses) {
        super(context, 0, courses);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Student student = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_view_student_item, parent, false);
        }
        // Lookup view for data population
        TextView tvFirstName = (TextView) convertView.findViewById(R.id.student_first_name);
        TextView tvSecondName = (TextView) convertView.findViewById(R.id.student_second_name);
        // Populate the data into the template view using the data object
        tvFirstName.setText(student.getFirstName());
        tvSecondName.setText(student.getLastName());
        // Return the completed view to render on screen
        return convertView;
    }
}