package radivoi.sasa.prezenta.service.student;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import radivoi.sasa.prezenta.R;
import radivoi.sasa.prezenta.domain.entity.Course;
import radivoi.sasa.prezenta.domain.entity.Student;
import radivoi.sasa.prezenta.service.presentation.Utils;

public class AddStudentActivity extends AppCompatActivity {

    private EditText firstNameT;
    private EditText secondNameT;
    private Course course;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_student);
        course = (Course) getIntent().getSerializableExtra(Utils.COURSE);

        //get text
        firstNameT = (EditText) findViewById(R.id.student_add_button_first_name);
        firstNameT.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (!textView.getText().toString().equals("")) {
                    return true;
                }
                return false;
            }
        });

        secondNameT = (EditText) findViewById(R.id.student_add_button_second_name);
        secondNameT.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (!textView.getText().toString().equals("")) {
                    return true;
                }
                return false;
            }
        });
        //button action
        Button mClickButtonCourse = (Button) findViewById(R.id.student_add_button_save);
        mClickButtonCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSave();
            }
        });
    }

    private void attemptSave() {

        // Reset errors.
        firstNameT.setError(null);
        secondNameT.setError(null);

        // Store values at the time of the save attempt.
        String firstName = firstNameT.getText().toString();
        String secondName = secondNameT.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid first name.
        if (TextUtils.isEmpty(secondName)) {
            secondNameT.setError(getString(R.string.error_invalid_student_first_name));
            focusView = secondNameT;
            cancel = true;
        }

        // Check for a valid second name
        if (TextUtils.isEmpty(firstName)) {
            firstNameT.setError(getString(R.string.error_invalid_student_second_name));
            focusView = firstNameT;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            Student student = new Student();
            student.setFirstName(firstName);
            student.setLastName(secondName);
            student.setCourse(course);
            student.save();
            finish();
        }
    }
}
