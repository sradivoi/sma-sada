package radivoi.sasa.prezenta.service.course;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.sql.Date;
import java.util.Calendar;

import radivoi.sasa.prezenta.R;
import radivoi.sasa.prezenta.domain.entity.Course;

public class AddCourseActivity extends AppCompatActivity {

    private EditText nameT;
    private EditText teacherT;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_course);

        //get text
        nameT = (EditText) findViewById(R.id.course_add_button_name);
        nameT.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (!textView.getText().toString().equals("")) {
                    return true;
                }
                return false;
            }
        });

        teacherT = (EditText) findViewById(R.id.course_add_button_teacher);
        teacherT.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (!textView.getText().toString().equals("")) {
                    return true;
                }
                return false;
            }
        });
        //button action
        Button mClickButtonCourse = (Button) findViewById(R.id.course_add_button_save);
        mClickButtonCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSave();
            }
        });
    }

    private void attemptSave() {

        // Reset errors.
        nameT.setError(null);
        teacherT.setError(null);

        // Store values at the time of the save attempt.
        String name = nameT.getText().toString();
        String teacher = teacherT.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid teacher name.
        if (TextUtils.isEmpty(teacher)) {
            teacherT.setError(getString(R.string.error_invalid_course_teacher));
            focusView = teacherT;
            cancel = true;
        }

        // Check for a valid course name
        if (TextUtils.isEmpty(name)) {
            nameT.setError(getString(R.string.error_invalid_course_name));
            focusView = nameT;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
        } else {
            Course course = new Course();
            course.setName(name);
            course.setTeacher(teacher);
            course.setDate(new Date(Calendar.getInstance().getTime().getTime()));
            course.save();
            finish();
        }
    }
}
