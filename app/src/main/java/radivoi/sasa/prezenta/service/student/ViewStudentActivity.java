package radivoi.sasa.prezenta.service.student;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.util.ArrayList;

import radivoi.sasa.prezenta.R;
import radivoi.sasa.prezenta.domain.entity.Course;
import radivoi.sasa.prezenta.domain.entity.Student;
import radivoi.sasa.prezenta.domain.repository.StudentRepository;
import radivoi.sasa.prezenta.service.presentation.StudentAdaptor;
import radivoi.sasa.prezenta.service.presentation.Utils;

public class ViewStudentActivity extends AppCompatActivity {

    private Course course;
    private StudentRepository studentRepository;
    private SwipeMenuCreator creator;
    private StudentAdaptor adapter;
    private SwipeMenuListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_student);
        course = (Course) getIntent().getSerializableExtra(Utils.COURSE);
        studentRepository = new StudentRepository(course);

        //button action
        Button mClickButtonAddStudent = (Button) findViewById(R.id.student_view_button_student);
        mClickButtonAddStudent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewStudentActivity.this, AddStudentActivity.class);
                intent.putExtra(Utils.COURSE, course);
                startActivity(intent);
            }
        });
        createSwipeMenu();
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        //Populate ListView
        updateList();
    }

    private void updateList() {
        // Find ListView to populate
        ArrayList<Student> arrayOfStudents = new ArrayList<>();
        arrayOfStudents = (ArrayList<Student>) studentRepository.getStudents();
        // Setup cursor adapter using cursor from last step
        adapter = new StudentAdaptor(this, arrayOfStudents);
        // Attach cursor adapter to the ListView
        listView = (SwipeMenuListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setMenuCreator(creator);

        // set SwipeListener
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // delete
                        adapter.getItem(position).delete();
                        adapter.remove(adapter.getItem(position));
                        adapter.notifyDataSetChanged();
                        break;
                }
                return false;
            }
        });
    }

    private void createSwipeMenu() {
        creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(90);
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
    }
}
