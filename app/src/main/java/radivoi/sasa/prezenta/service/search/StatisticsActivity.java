package radivoi.sasa.prezenta.service.search;

import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import radivoi.sasa.prezenta.R;
import radivoi.sasa.prezenta.domain.entity.Course;
import radivoi.sasa.prezenta.domain.repository.CourseRepository;

public class StatisticsActivity extends AppCompatActivity implements View.OnClickListener {

    private static String DATE_FORMAT = "dd-MM-yyyy";

    //UI References
    private EditText fromDateEtxt;
    private EditText toDateEtxt;
    private EditText firstName;
    private EditText lastName;

    private DatePickerDialog fromDatePickerDialog;
    private DatePickerDialog toDatePickerDialog;

    private Button resultButton;

    private SimpleDateFormat dateFormatter;

    private final CourseRepository courseRepository = new CourseRepository();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistics);

        dateFormatter = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);

        findViewsById();

        setDateTimeField();
    }

    @Override
    public void onClick(View view) {
        if (view == fromDateEtxt) {
            fromDatePickerDialog.show();
        } else if (view == toDateEtxt) {
            toDatePickerDialog.show();
        }
    }

    private void findViewsById() {
        firstName = (EditText) findViewById(R.id.statistics_first_name);
        lastName = (EditText) findViewById(R.id.statistics_last_name);
        fromDateEtxt = (EditText) findViewById(R.id.statistics_fromdate);
        fromDateEtxt.setInputType(InputType.TYPE_NULL);
        fromDateEtxt.requestFocus();

        toDateEtxt = (EditText) findViewById(R.id.statistics_todate);
        toDateEtxt.setInputType(InputType.TYPE_NULL);

        resultButton = (Button) findViewById(R.id.statistics_button);
        resultButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (attemptResult())
                    showResult();
            }
        });
    }

    private void setDateTimeField() {
        fromDateEtxt.setOnClickListener(this);
        toDateEtxt.setOnClickListener(this);

        Calendar newCalendar = Calendar.getInstance();
        fromDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                fromDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        toDatePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                toDateEtxt.setText(dateFormatter.format(newDate.getTime()));
            }

        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
    }

    private int calculateAttendance() {
        Date fromDate = transformToDate(fromDateEtxt.getText().toString(), this.DATE_FORMAT);
        Date toDate = transformToDate(toDateEtxt.getText().toString(), this.DATE_FORMAT);
        String firstN = firstName.getText().toString();
        String lastN = lastName.getText().toString();
        List<Course> coursesWithSearchedStudents = courseRepository.getCourseBetween(fromDate,
                toDate,
                firstN,
                lastN);
        return coursesWithSearchedStudents.size();
    }

    private Date transformToDate(String date, String formatPattern) {
        SimpleDateFormat format = new SimpleDateFormat(formatPattern, Locale.ENGLISH);
        java.util.Date parsed = null;
        try {
            parsed = format.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert parsed != null;
        return new java.sql.Date(parsed.getTime());
    }

    private void showResult() {

        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setTitle("Statistics");
        builder1.setMessage(firstName.getText().toString() + " has attended " + calculateAttendance() + " time.");
        builder1.setCancelable(true);
        builder1.setNeutralButton(android.R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    private boolean attemptResult() {

        // Reset errors.
        fromDateEtxt.setError(null);
        toDateEtxt.setError(null);
        firstName.setError(null);
        lastName.setError(null);

        // Store values at the time of the save attempt.
        String fistNameT = firstName.getText().toString();
        String lastNameT = lastName.getText().toString();
        String toDateT = toDateEtxt.getText().toString();
        String fromDateT = fromDateEtxt.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid last name
        if (TextUtils.isEmpty(lastNameT)) {
            lastName.setError(getString(R.string.error_invalid_lastName));
            focusView = lastName;
            cancel = true;
        }

        // Check for a valid first name
        if (TextUtils.isEmpty(fistNameT)) {
            firstName.setError(getString(R.string.error_invalid_firstName));
            focusView = firstName;
            cancel = true;
        }

        // Check for a valid to date
        if (TextUtils.isEmpty(toDateT)) {
            toDateEtxt.setError(getString(R.string.error_invalid_toDate));
            focusView = toDateEtxt;
            cancel = true;
        }

        // Check for a valid first name
        if (TextUtils.isEmpty(fromDateT)) {
            fromDateEtxt.setError(getString(R.string.error_invalid_fromDate));
            focusView = fromDateEtxt;
            cancel = true;
        }

        if (cancel) {
            // There was an error; don't attempt login and focus the first
            // form field with an error.
            focusView.requestFocus();
            return false;
        } else {
            return true;
        }
    }
}
