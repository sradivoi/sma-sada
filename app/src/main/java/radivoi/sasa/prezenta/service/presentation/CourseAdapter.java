package radivoi.sasa.prezenta.service.presentation;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

import radivoi.sasa.prezenta.R;
import radivoi.sasa.prezenta.domain.entity.Course;

/**
 * Created by I335330 on 07/01/2017.
 */

public class CourseAdapter extends ArrayAdapter<Course> {
    public CourseAdapter(Context context, ArrayList<Course> courses) {
        super(context, 0, courses);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Course course = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_view_course_item, parent, false);
        }
        // Lookup view for data population
        TextView tvName = (TextView) convertView.findViewById(R.id.course_name);
        TextView tvTeacher = (TextView) convertView.findViewById(R.id.course_teacher);
        TextView tvDate = (TextView) convertView.findViewById(R.id.course_date);
        // Populate the data into the template view using the data object
        tvName.setText(course.getName());
        tvTeacher.setText(course.getTeacher());
        tvDate.setText(android.text.format.DateFormat.format("yyyy-MM-dd hh:mm:ss a", course.getDate()).toString());
        // Return the completed view to render on screen
        return convertView;
    }
}
