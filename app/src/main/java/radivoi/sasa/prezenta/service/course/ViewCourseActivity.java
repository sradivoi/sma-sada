package radivoi.sasa.prezenta.service.course;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.util.ArrayList;

import radivoi.sasa.prezenta.R;
import radivoi.sasa.prezenta.domain.entity.Course;
import radivoi.sasa.prezenta.service.presentation.CourseAdapter;
import radivoi.sasa.prezenta.service.presentation.Utils;
import radivoi.sasa.prezenta.service.student.ViewStudentActivity;

public class ViewCourseActivity extends AppCompatActivity {

    private SwipeMenuCreator creator;
    private CourseAdapter adapter;
    private SwipeMenuListView listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_course);

        //button action
        Button mClickButtonAddCourse = (Button) findViewById(R.id.course_view_button_course);
        mClickButtonAddCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ViewCourseActivity.this, AddCourseActivity.class);
                startActivity(intent);
            }
        });
        createSwipeMenu();
    }

    private void createSwipeMenu() {
        creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0xC9, 0xC9,
                        0xCE)));
                // set item width
                openItem.setWidth(90);
                // set item title
                openItem.setTitle("Open");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);

                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(90);
                // set a icon
                deleteItem.setIcon(R.drawable.ic_delete);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };
    }

    @Override
    public void onResume() {  // After a pause OR at startup
        super.onResume();
        //Populate ListView
        updateList();
    }

    private void updateList() {
        // Find ListView to populate
        ArrayList<Course> arrayOfCourses = new ArrayList<>();
        arrayOfCourses = (ArrayList<Course>) SQLite.select().
                from(Course.class).queryList();
        // Setup cursor adapter using cursor from last step
        adapter = new CourseAdapter(this, arrayOfCourses);
        // Attach cursor adapter to the ListView
        listView = (SwipeMenuListView) findViewById(R.id.listView);
        listView.setAdapter(adapter);
        listView.setMenuCreator(creator);

        // set SwipeListener
        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        // open
                        Intent intent = new Intent(ViewCourseActivity.this, ViewStudentActivity.class);
                        //based on item add info to intent
                        Course selectedCourse = adapter.getItem(position);
                        intent.putExtra(Utils.COURSE, selectedCourse);
                        startActivity(intent);
                        break;
                    case 1:
                        // delete
                        adapter.getItem(position).delete();
                        adapter.remove(adapter.getItem(position));
                        adapter.notifyDataSetChanged();
                        break;
                }
                return false;
            }
        });
    }
}
