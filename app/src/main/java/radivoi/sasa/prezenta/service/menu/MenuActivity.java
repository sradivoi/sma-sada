package radivoi.sasa.prezenta.service.menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import radivoi.sasa.prezenta.R;
import radivoi.sasa.prezenta.service.course.ViewCourseActivity;
import radivoi.sasa.prezenta.service.search.StatisticsActivity;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //buttons actions
        Button mClickButtonCourse = (Button) findViewById(R.id.menu_button_course);
        mClickButtonCourse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, ViewCourseActivity.class);
                startActivity(intent);
            }
        });

        Button mClickButtonStats = (Button) findViewById(R.id.menu_button_stats);
        mClickButtonStats.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuActivity.this, StatisticsActivity.class);
                startActivity(intent);
            }
        });

    }
}
