package radivoi.sasa.prezenta.domain.persistance;

import com.raizlabs.android.dbflow.annotation.Database;

/**
 * Created by I335330 on 07/01/2017.
 */

@Database(name = CustomDatabase.NAME, version = CustomDatabase.VERSION)
public class CustomDatabase {
    public static final String NAME = "DataBase";

    public static final int VERSION = 2;
}
