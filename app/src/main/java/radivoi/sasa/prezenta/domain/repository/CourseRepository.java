package radivoi.sasa.prezenta.domain.repository;

import com.raizlabs.android.dbflow.sql.language.Join;
import com.raizlabs.android.dbflow.sql.language.SQLite;

import java.sql.Date;
import java.util.List;

import radivoi.sasa.prezenta.domain.entity.Course;
import radivoi.sasa.prezenta.domain.entity.Course_Table;
import radivoi.sasa.prezenta.domain.entity.Student;
import radivoi.sasa.prezenta.domain.entity.Student_Table;

/**
 * Created by I335330 on 08/01/2017.
 */

public class CourseRepository {

    public List<Course> getCourseBetween(Date fromDate, Date toDate, String firstName, String lastName) {
        return SQLite.select()
                .from(Course.class).join(Student.class, Join.JoinType.INNER)
                .on(Course_Table.uid.withTable().eq(Student_Table.course_uid.withTable()))
                .where(Course_Table.date.withTable().between(fromDate).and(toDate))
                .and(Student_Table.firstName.like(firstName))
                .and(Student_Table.lastName.like(lastName))
                .queryList();
    }
}
