package radivoi.sasa.prezenta.domain.entity;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.ForeignKey;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;

import radivoi.sasa.prezenta.domain.persistance.CustomDatabase;

/**
 * Created by I335330 on 07/01/2017.
 */

@Table(database = CustomDatabase.class)
public class Student extends BaseModel implements Serializable{

    @ForeignKey(stubbedRelationship = true)
    Course course;
    @Column
    @PrimaryKey(autoincrement = true)
    private long uid;
    @Column
    private String firstName;
    @Column
    private String lastName;

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
