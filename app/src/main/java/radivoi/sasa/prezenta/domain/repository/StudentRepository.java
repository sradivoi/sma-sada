package radivoi.sasa.prezenta.domain.repository;

import java.util.List;

import radivoi.sasa.prezenta.domain.entity.Course;
import radivoi.sasa.prezenta.domain.entity.Student;

/**
 * Created by I335330 on 08/01/2017.
 */

public class StudentRepository {

    private final Course course;

    public StudentRepository(Course course) {
        this.course = course;
    }

    public Course getCourse() {
        return course;
    }

    public List<Student> getStudents() {
        return course.getStudents();
    }
}
