package radivoi.sasa.prezenta.domain.entity;

import com.raizlabs.android.dbflow.annotation.Column;
import com.raizlabs.android.dbflow.annotation.OneToMany;
import com.raizlabs.android.dbflow.annotation.PrimaryKey;
import com.raizlabs.android.dbflow.annotation.Table;
import com.raizlabs.android.dbflow.sql.language.Condition;
import com.raizlabs.android.dbflow.sql.language.NameAlias;
import com.raizlabs.android.dbflow.sql.language.SQLite;
import com.raizlabs.android.dbflow.structure.BaseModel;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import radivoi.sasa.prezenta.domain.persistance.CustomDatabase;

/**
 * Created by I335330 on 07/01/2017.
 */

@Table(database = CustomDatabase.class)
public class Course extends BaseModel implements Serializable {

    public List<Student> students;
    @Column
    @PrimaryKey(autoincrement = true)
    private long uid;
    @Column
    private String name;
    @Column
    private String teacher;
    @Column
    private Date date;

    @OneToMany(methods = OneToMany.Method.ALL, variableName = "students")
    public List<Student> getStudents() {
            students = SQLite.select()
                    .from(Student.class)
                    .where(Student_Table.course_uid.eq(uid))
                    .queryList();
        return students;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTeacher() {
        return teacher;
    }

    public void setTeacher(String teacher) {
        this.teacher = teacher;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public long getId() {
        return uid;
    }

    public long getUid() {
        return uid;
    }

    public void setUid(long uid) {
        this.uid = uid;
    }
}
